/*
程序说明：解码红外遥控器
*/

#include <IRremote.h>

int RECV_PIN = 11; //红外线接收器OUTPUT端接在pin 11
IRrecv irrecv(RECV_PIN); // 定义IRrecv 对象来接收红外线信号
decode_results results; //解码结果放在decode_results构造的对象results里

void setup(){
  Serial.begin(9600);
  irrecv.enableIRIn(); // 启动红外解码
}

void loop() {
  if (irrecv.decode(&results)) { // 解码成功，收到一组红外线信号
    Serial.print("irCode: "); 
    Serial.print(results.value,HEX); // 输出红外线解码结果（十六进制）
    //results.value 是unsigned long型，头文件有介绍
    delay(100);  //按键消抖
    Serial.print(", bits: "); 
    Serial.println(results.bits); // 红外线码元位数 
    irrecv.resume();
  }
}
