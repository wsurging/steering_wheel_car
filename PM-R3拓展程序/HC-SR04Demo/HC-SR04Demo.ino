#include <Servo.h>            //添加舵机库
Servo myservo;
Servo myservo1;                //定义2个舵机
int Echo = A4;                 // Echo回声脚
int Trig = A5;                 // Trig 触发脚
int PWM = 5;                   //定义电机PWM引脚
int DIR = 4;                   //定义电机方向引脚
int val = 60;                  //电机速度(0-255),
 
int rightDistance = 0,leftDistance = 0,midDistance = 0 ; 
 
int Distance()               // 量出前方距离 
{
  digitalWrite(Trig, LOW);        // 给触发脚低电平2μs
  delayMicroseconds(2);
  digitalWrite(Trig, HIGH);       // 给触发脚高电平10μs，这里至少是10μs
  delayMicroseconds(10);
  digitalWrite(Trig, LOW);        // 持续给触发脚低电
  float Fdistance = pulseIn(Echo, HIGH);  // 读取高电平时间(单位：微秒)
  Fdistance= Fdistance/58;       //为什么除以58等于厘米，  Y米=（X秒*344）/2
  // X秒=（ 2*Y米）/344 ==》X秒=0.0058*Y米 ==》厘米=微秒/58
  return (int)Fdistance;
}  

void Forward(){                    //前进
  digitalWrite(DIR,LOW);
  analogWrite(PWM, val);
  myservo.write(90);
}
void Back(){                       //后退
  digitalWrite(DIR,HIGH);
  analogWrite(PWM, val);
  myservo.write(90);
}
void Left(){                       //左转
  digitalWrite(DIR,LOW);
  analogWrite(PWM, val);
  myservo.write(35);
}
void Right(){                      //右转
  digitalWrite(DIR,LOW);
  analogWrite(PWM, val);
  myservo.write(120);
}
void Stop(){                       //停止
  digitalWrite(DIR,LOW);
  analogWrite(PWM, LOW);
  myservo.write(90);
}

void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600);             // 初始化串口
  pinMode(Echo, INPUT);            // 定义超声波输入脚
  pinMode(Trig, OUTPUT);           // 定义超声波输出脚  
  pinMode(PWM,OUTPUT);
  pinMode(DIR,OUTPUT);
  myservo.attach(3);               //转向舵机引脚
  myservo1.attach(8);              //超声波舵机引脚
  myservo.write(90);               //转向舵机初始角度
  myservo1.write(90);              //超声波云台舵机初始角度
  delay(1000);

}

void loop() {
  // put your main code here, to run repeatedly:
  midDistance = Distance();           //测试前方距离
  delay(100);
  Serial.print("midDistance=");
  Serial.println(midDistance);
  if(midDistance < 55){               //小于55厘米停止
    Stop();
    Serial.println("STOP");
    myservo1.write(0);                 //舵机右转
    delay(1000);
    rightDistance = Distance();         //记录右侧距离
    Serial.print("leftDistance=");
    Serial.println(leftDistance);
    myservo1.write(180);               //左转
    delay(1500);
    leftDistance = Distance();        //记录左侧距离
    Serial.print("rightDistance=");
    Serial.println(rightDistance);
    myservo1.write(90);                //舵机回中
    delay(100);
    //判断左右距离，都小于20cm后退或者前方距离小于45cm
    if(midDistance < 45 || leftDistance <= 20 && rightDistance <= 20){    
      Back();
      delay(1000);
      Serial.println("BACK");
      //左边距离大于右边距离并且左右都大于45cm，左转
    }else if(leftDistance > rightDistance && leftDistance > 45){    
      Left();
      delay(2000);
      Serial.println("LEFT");
      //左边距离小于右边距离并且左右都大于45cm，右转
    }else if(leftDistance < rightDistance && rightDistance > 45){ 
      Right();
      delay(2000);
      Serial.println("RIGHT");
    }
  }else if(midDistance > 55){         //前方距离大于55cm,前进
    Forward();
    Serial.println("FORWARD");
  }else
  Forward();
}
