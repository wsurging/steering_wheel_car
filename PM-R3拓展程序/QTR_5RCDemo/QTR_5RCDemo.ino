#include <Servo.h>
#include <QTRSensors.h>
Servo myservo;

#define NUM_SENSORS   5     // number of sensors used
#define TIMEOUT       2500  // waits for 2500 microseconds for sensor outputs to go low
#define EMITTER_PIN   2     // emitter is controlled by digital pin 2
#define servo  3          //servo pin
#define DIR    4          //motor dir pin
#define PWM    5          //motor pwm pin
#define LED    13
#define Val    50         //motor speed

#define servoMin  50       //turn left max angle
#define servoMid  90       //Straight angle
#define servoMax  130      //turn right max angle

// sensors 1 through 5 are connected to digital pins A0 through A4, respectively
QTRSensorsRC qtrrc((unsigned char[]) {14, 15, 16, 17, 18},NUM_SENSORS, TIMEOUT, EMITTER_PIN); 
unsigned int sensorValues[NUM_SENSORS];


void setup()
{
  delay(500);
  myservo.attach(servo);
  pinMode(LED, OUTPUT);
  pinMode(DIR, OUTPUT);
  pinMode(PWM, OUTPUT);
  digitalWrite(LED, HIGH);    // turn on Arduino's LED to indicate we are in calibration mode
  for (int i = 0; i < 400; i++)  // make the calibration take about 10 seconds
  {
    qtrrc.calibrate();       // reads all sensors 10 times at 2500 us per read (i.e. ~25 ms per call)
  }
  digitalWrite(LED, LOW);     // turn off Arduino's LED to indicate we are through with calibration

  // print the calibration minimum values measured when emitters were on
  Serial.begin(9600);
  for (int i = 0; i < NUM_SENSORS; i++)
  {
    Serial.print(qtrrc.calibratedMinimumOn[i]);
    Serial.print(' ');
  }
  Serial.println();
  
  // print the calibration maximum values measured when emitters were on
  for (int i = 0; i < NUM_SENSORS; i++)
  {
    Serial.print(qtrrc.calibratedMaximumOn[i]);
    Serial.print(' ');
  }
  Serial.println();
  Serial.println();
  delay(1000);
  myservo.write(servoMid); 
}

void loop()
{
  //Get the sensor value
  int IR1 = 0;   IR1 = analogRead(14);
  int IR2 = 0;   IR2 = analogRead(15);
  int IR3 = 0;   IR3 = analogRead(16);
  int IR4 = 0;   IR4 = analogRead(17);
  int IR5 = 0;   IR5 = analogRead(18);

  //qtrrc.read(sensorValues); 
  //instead of unsigned int position = qtrrc.readLine(sensorValues);
  unsigned int position = qtrrc.readLine(sensorValues);

  for (unsigned char i = 0; i < NUM_SENSORS; i++)
  {
    Serial.print(sensorValues[i]);
    Serial.print('\t');
  }
  //Serial.println(); // uncomment this line if you are using raw values
  Serial.println(position); // comment this line out if you are using raw values

//Determine the sensor value, less than 2000 to the right, 
//greater than 2000 to the left, 2000 straight.
  if(position < 500){
    digitalWrite(DIR,LOW);
    analogWrite(PWM,Val);
    myservo.write(servoMax);
  }else if(position >= 500 && position < 2000){
    digitalWrite(DIR,LOW);
    analogWrite(PWM,Val);
    myservo.write(map(position, 1999, 1000, 91, 110));
  }else if(position > 2000 && position <= 3500){
    digitalWrite(DIR,LOW);
    analogWrite(PWM,Val);
    myservo.write(map(position, 2001, 3500, 89, 55));
  }else if(position > 3500){
    digitalWrite(DIR,LOW);
    analogWrite(PWM,Val);
    myservo.write(servoMin);
  }else{
    digitalWrite(DIR,LOW);
    analogWrite(PWM,Val);
    myservo.write(servoMid);
  }
}

